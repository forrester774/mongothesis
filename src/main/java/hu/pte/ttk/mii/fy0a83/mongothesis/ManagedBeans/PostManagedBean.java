package hu.pte.ttk.mii.fy0a83.mongothesis.ManagedBeans;


import hu.pte.ttk.mii.fy0a83.mongothesis.Entities.Image;
import hu.pte.ttk.mii.fy0a83.mongothesis.Entities.Post;
import hu.pte.ttk.mii.fy0a83.mongothesis.Entities.Social;
import hu.pte.ttk.mii.fy0a83.mongothesis.Entities.User;
import hu.pte.ttk.mii.fy0a83.mongothesis.Repositories.PostRepository;
import hu.pte.ttk.mii.fy0a83.mongothesis.Repositories.SocialRepository;
import hu.pte.ttk.mii.fy0a83.mongothesis.Repositories.UsersRepository;
import lombok.Getter;
import lombok.Setter;
import org.primefaces.event.FileUploadEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.util.*;

@Named
@SessionScoped
public class PostManagedBean {

    @Autowired
    private PostRepository postRepository;

    @Autowired
    private SocialRepository socialRepository;

    @Autowired
    private UsersRepository usersRepository;

    @Getter
    private List<String> topics;

    @Getter
    private User user;

    @Getter
    @Setter
    private Post post;

    @PostConstruct
    public void init() {
        post = new Post();
        topics = findAllTopics();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        user = usersRepository.findByUserName(auth.getName());
    }

    public void savePost(Post post) {
        try {
            if (post.getId() == null) {
                post.setCreatedDate(new Date());
                post.setLastModifiedDate(new Date());
                post.setPoster(user);
                postRepository.save(post);
                topics = findAllTopics();
                addMessage("Post Saved!");
            } else {
                post.setLastModifiedDate(new Date());
                postRepository.save(post);
                topics = findAllTopics();
                addMessage("Post Updated!");
            }
        } catch (Exception e) {
            addMessage("Error saving post! Error message:" + e);
        }
    }

    public void updatePost(String id) {
        try {
            post = postRepository.findOne(id);

            ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();

            Map<String, Object> requestMap = externalContext.getRequestMap();
            requestMap.put("post", post);
            FacesContext.getCurrentInstance().getExternalContext().redirect("http://mongothesis-env.qkvfhgmmym.eu-west-1.elasticbeanstalk.com/editPost.xhtml");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void openPost(String id) {
        try {
            post = postRepository.findOne(id);
            ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
            Map<String, Object> requestMap = externalContext.getRequestMap();
            requestMap.put("posts", post);
            FacesContext.getCurrentInstance().getExternalContext().redirect("http://mongothesis-env.qkvfhgmmym.eu-west-1.elasticbeanstalk.com/viewPost.xhtml");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void newPost() throws Exception {
        try {
            post = new Post();
            Social social = new Social();
            Map<String, String> votedMap = new HashMap<>();
            social.setVoted(votedMap);
            post.setComments(new ArrayList<>());
            socialRepository.save(social);
            post.setSocial(social);
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().clear();
            FacesContext.getCurrentInstance().getExternalContext().redirect("http://mongothesis-env.qkvfhgmmym.eu-west-1.elasticbeanstalk.com/editPost.xhtml");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void myPosts() {
        try {
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().clear();
            FacesContext.getCurrentInstance().getExternalContext().redirect("http://mongothesis-env.qkvfhgmmym.eu-west-1.elasticbeanstalk.com/myPosts.xhtml");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addMessage(String summary) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, null);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    public void imageUpload(FileUploadEvent event) {
        try {
            post.setImage(new Image(event.getFile().getContents(), event.getFile().getFileName()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void dismissImage() {
        post.setImage(null);
    }

    public String getImageContentsAsBase64(String id) {
        post = postRepository.findOne(id);
        if (post.getImage() != null) {
            return Base64.getEncoder().encodeToString(post.getImage().getBytes());
        }
        return null;
    }

    public boolean hasImage(String id) {
        post = postRepository.findOne(id);
        return post.getImage() != null;
    }

    private void simpleSave(Post post) {
        postRepository.save(post);
    }

    public void upvote(String id) {
        try {
            post = postRepository.findOne(id);
            Social social = socialRepository.findOne(post.getSocial().getId());
            Map<String, String> votedMap = social.getVoted();

            if (votedMap.containsKey(user.getId()) && votedMap.get(user.getId()).equals("upvote")) {
                social.setUpvotes(social.getUpvotes() - 1);
                votedMap.remove(user.getId());

            } else if(votedMap.containsKey(user.getId()) && votedMap.get(user.getId()).equals("downvote")){
              social.setUpvotes(social.getUpvotes() + 1);
              social.setDownvotes(social.getDownvotes() - 1);
              votedMap.remove(user.getId());
              votedMap.put(user.getId(), "upvote");

            } else {
                votedMap.put(user.getId(), "upvote");
                social.setVoted(votedMap);
                social.setUpvotes(social.getUpvotes() + 1);
            }
            post.setSocial(social);
            socialRepository.save(social);
            simpleSave(post);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void downvote(String id) {
        try {
            post = postRepository.findOne(id);
            Social social = socialRepository.findOne(post.getSocial().getId());
            Map<String, String> votedMap = social.getVoted();
            if (votedMap.containsKey(user.getId()) && votedMap.get(user.getId()).equals("downvote")) {
                social.setDownvotes(social.getDownvotes() - 1);
                votedMap.remove(user.getId());
            }
            else if(votedMap.containsKey(user.getId()) && votedMap.get(user.getId()).equals("upvote")){
                social.setUpvotes(social.getUpvotes() - 1);
                social.setDownvotes(social.getDownvotes() + 1);
                votedMap.remove(user.getId());
                votedMap.put(user.getId(), "downvote");

            } else {
                votedMap.put(user.getId(), "downvote");
                social.setVoted(votedMap);
                social.setDownvotes(social.getDownvotes() + 1);
            }

            post.setSocial(social);
            socialRepository.save(social);
            simpleSave(post);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private List<String> findAllTopics() {
        List<Post> posts = postRepository.findAll();

        List<String> toReturn = new ArrayList<>();
        for (Post post : posts) {
            for (String topic : post.getTopics()) {
                if (!toReturn.contains(topic)) {
                    toReturn.add(topic);
                }
            }
        }
        return toReturn;
    }
}
