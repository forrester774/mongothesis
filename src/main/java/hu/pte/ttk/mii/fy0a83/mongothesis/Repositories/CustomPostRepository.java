package hu.pte.ttk.mii.fy0a83.mongothesis.Repositories;

import hu.pte.ttk.mii.fy0a83.mongothesis.Entities.Post;

import java.util.List;

public interface CustomPostRepository {

    List<Post> upvotes();

    List<Post> searchByKeyword(String keyword);
}
