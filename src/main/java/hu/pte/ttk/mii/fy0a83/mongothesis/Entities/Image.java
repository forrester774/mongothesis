package hu.pte.ttk.mii.fy0a83.mongothesis.Entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Image extends AbstractEntity implements Serializable {

    private byte[] bytes;

    private String fileName;
}
