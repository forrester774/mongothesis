package hu.pte.ttk.mii.fy0a83.mongothesis.Security;

import com.mongodb.Mongo;
import com.mongodb.MongoURI;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.core.mapping.event.ValidatingMongoEventListener;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

@Configuration
public class MoMongoConfig extends AbstractMongoConfiguration {

    @Bean
    public ValidatingMongoEventListener validatingMongoEventListener() {
        return new ValidatingMongoEventListener(validator());
    }

    @Bean
    public LocalValidatorFactoryBean validator() {
        return new LocalValidatorFactoryBean();
    }

    @Override
    protected String getDatabaseName() {
        return "test";
    }

    @Override
    public Mongo mongo() throws Exception {
        MongoURI mongoURI = new MongoURI("mongodb://admin:AuFzdN7Rgnx293v@cluster0-shard-00-00-jnz0e.mongodb.net:27017,cluster0-shard-00-01-jnz0e.mongodb.net:27017,cluster0-shard-00-02-jnz0e.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true");
        return new Mongo(mongoURI);
    }}
