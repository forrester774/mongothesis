package hu.pte.ttk.mii.fy0a83.mongothesis.Entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "Users")
public class User extends AbstractEntity implements Serializable {


    @Indexed
    private String name;

    @NotEmpty(message = "Please enter a username!")
    private String userName;

    @Length(min = 5, message = "The password must have at least 5 characters!")
    @NotEmpty(message = "Please enter a password!")
    private String password;

    private String email;
}
