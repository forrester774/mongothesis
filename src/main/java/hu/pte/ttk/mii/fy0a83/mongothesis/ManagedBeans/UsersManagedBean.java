package hu.pte.ttk.mii.fy0a83.mongothesis.ManagedBeans;

import hu.pte.ttk.mii.fy0a83.mongothesis.Entities.Post;
import hu.pte.ttk.mii.fy0a83.mongothesis.Entities.Social;
import hu.pte.ttk.mii.fy0a83.mongothesis.Entities.User;
import hu.pte.ttk.mii.fy0a83.mongothesis.Repositories.PostRepository;
import hu.pte.ttk.mii.fy0a83.mongothesis.Repositories.UsersRepository;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.faces.bean.ApplicationScoped;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.Date;

@Named
@ApplicationScoped
public class UsersManagedBean {

    @Autowired
    private PostRepository postRepository;

    @Autowired
    private UsersRepository usersRepository;


    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }


    public void init() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = usersRepository.findByUserName(auth.getName());

       for (int i = 0; i< 10000; i++) {
           Post post = new Post(RandomStringUtils.random(10, "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"),
                   " description", "content", new ArrayList<>(), new Social(),
                   user, new ArrayList<>(), null, new Date());
           postRepository.save(post);
           System.out.println(post.getTitle());
       }
        System.out.println("DONE");
    }


}
