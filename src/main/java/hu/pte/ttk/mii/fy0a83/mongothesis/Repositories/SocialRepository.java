package hu.pte.ttk.mii.fy0a83.mongothesis.Repositories;

import hu.pte.ttk.mii.fy0a83.mongothesis.Entities.Social;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface SocialRepository extends MongoRepository<Social, String> {

}
