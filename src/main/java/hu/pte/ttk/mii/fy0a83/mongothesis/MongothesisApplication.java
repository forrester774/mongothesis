package hu.pte.ttk.mii.fy0a83.mongothesis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MongothesisApplication {

    public static void main(String[] args) {
        SpringApplication.run(MongothesisApplication.class, args);
    }
}
