package hu.pte.ttk.mii.fy0a83.mongothesis.Entities;

import lombok.*;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.mongodb.core.index.TextIndexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Document(collection = "Posts")
@NoArgsConstructor
@AllArgsConstructor
public class Post extends AbstractEntity implements Serializable {

    @NotEmpty(message = "Every post must have a title!")
    @TextIndexed
    @Size(max = 20)
    private String title;

    @NotEmpty(message = "Every post must have a description!")
    @TextIndexed
    @Size(max = 100)
    private String description;

    @TextIndexed
    private String content;

    @DBRef(lazy = true)
    private List<Comment> comments;

    private Social social;

    private User poster;

    @NotEmpty(message = "Every post must have at least one topic!")
    private List<String> topics;

    private Image image;

    private Date postCreated;
}
