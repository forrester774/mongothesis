package hu.pte.ttk.mii.fy0a83.mongothesis.Security;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.io.IOException;

@Named
public class LoginBean {

    public void onPageLoad() {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (!(auth instanceof AnonymousAuthenticationToken)) {
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("http://mongothesis-env.qkvfhgmmym.eu-west-1.elasticbeanstalk.com/index.xhtml");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void logout() {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (!(auth instanceof AnonymousAuthenticationToken)) {
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("http://mongothesis-env.qkvfhgmmym.eu-west-1.elasticbeanstalk.com/logout");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
