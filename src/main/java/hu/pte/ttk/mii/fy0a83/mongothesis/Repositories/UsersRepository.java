package hu.pte.ttk.mii.fy0a83.mongothesis.Repositories;

import hu.pte.ttk.mii.fy0a83.mongothesis.Entities.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsersRepository extends MongoRepository<User, String> {

    User findByUserName(String username);
}
