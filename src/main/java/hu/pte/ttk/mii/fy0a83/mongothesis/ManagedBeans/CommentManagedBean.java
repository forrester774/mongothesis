package hu.pte.ttk.mii.fy0a83.mongothesis.ManagedBeans;

import hu.pte.ttk.mii.fy0a83.mongothesis.Entities.Comment;
import hu.pte.ttk.mii.fy0a83.mongothesis.Entities.Post;
import hu.pte.ttk.mii.fy0a83.mongothesis.Entities.User;
import hu.pte.ttk.mii.fy0a83.mongothesis.Repositories.CommentRepository;
import hu.pte.ttk.mii.fy0a83.mongothesis.Repositories.PostRepository;
import hu.pte.ttk.mii.fy0a83.mongothesis.Repositories.UsersRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.util.Date;
import java.util.List;

@Named
@ViewScoped
public class CommentManagedBean {

    @Autowired
    private PostRepository postRepository;

    @Autowired
    private CommentRepository commentRepository;

    @Autowired
    private UsersRepository usersRepository;

    @Getter
    @Setter
    private Comment comment;

    @Getter
    User user;

    @PostConstruct
    public void init() {
        comment = new Comment();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        user = usersRepository.findByUserName(auth.getName());
    }

    public void saveComment(Comment comment, String id) {
        try {
            Post post = postRepository.findOne(id);
            comment.setCommenter(user);
            comment.setCreatedDate(new Date());
            comment.setLastModifiedDate(new Date());
            commentRepository.save(comment);

            post.getComments().add(comment);
            postRepository.save(post);
            this.comment = new Comment();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteComment(Comment comment, String id) {
        try {
            Post post = postRepository.findOne(id);
            List<Comment> commentList = post.getComments();
            for (Comment comment1 : commentList) {
                if (comment1.getId().equals(comment.getId())) {
                    commentList.remove(comment1);
                    break;
                }
            }
            post.setComments(commentList);
            postRepository.save(post);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
