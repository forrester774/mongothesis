package hu.pte.ttk.mii.fy0a83.mongothesis.Repositories;

import hu.pte.ttk.mii.fy0a83.mongothesis.Entities.Post;
import hu.pte.ttk.mii.fy0a83.mongothesis.Entities.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PostRepository extends MongoRepository<Post, String> {

    List<Post> findAllByTopicsContainsOrderByCreatedDate(String topic);

    List<Post> findAllByPosterEquals(User user);

    List<Post> findAllByOrderByCreatedDateDesc();
}
