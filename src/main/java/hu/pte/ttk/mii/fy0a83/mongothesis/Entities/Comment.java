package hu.pte.ttk.mii.fy0a83.mongothesis.Entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Comment extends AbstractEntity implements Serializable {

    @Size(max = 100)
    private String commentBody;

    private User commenter;

    private Date commentPosted;

}
