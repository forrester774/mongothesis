package hu.pte.ttk.mii.fy0a83.mongothesis.Repositories;

import hu.pte.ttk.mii.fy0a83.mongothesis.Entities.Comment;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CommentRepository extends MongoRepository<Comment, String> {

}
