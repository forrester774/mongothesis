package hu.pte.ttk.mii.fy0a83.mongothesis.Entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Map;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Social extends AbstractEntity implements Serializable {

    private int upvotes;

    private int downvotes;

    private Map<String, String> voted;
}
