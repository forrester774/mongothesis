package hu.pte.ttk.mii.fy0a83.mongothesis.ManagedBeans;


import hu.pte.ttk.mii.fy0a83.mongothesis.Entities.Post;
import hu.pte.ttk.mii.fy0a83.mongothesis.Entities.User;
import hu.pte.ttk.mii.fy0a83.mongothesis.Repositories.PostRepository;
import hu.pte.ttk.mii.fy0a83.mongothesis.Repositories.UsersRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;

@Named
@ViewScoped
public class MyPostsManagedBean {

    @Getter
    @Autowired
    private PostRepository postRepository;

    @Getter
    @Setter
    @DBRef(lazy = true)
    private List<Post> postsList;

    @Getter
    private User user;

    @Autowired
    private UsersRepository usersRepository;

    @PostConstruct
    public void init() {
        postsList = new ArrayList<>();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        user = usersRepository.findByUserName(auth.getName());
        loadPosts();

    }

    private void loadPosts() {
        postsList = postRepository.findAllByPosterEquals(this.getUser());
    }

    public void deletePost(String id) {
        try {
            postRepository.delete(id);
            loadPosts();
            addMessage("Post Deleted!");
        } catch (Exception e) {
            addMessage("Error deleting post! Error message: " + e);
        }
    }

    private void addMessage(String summary) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, null);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }
}
