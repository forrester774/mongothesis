package hu.pte.ttk.mii.fy0a83.mongothesis.Repositories.impl;

import hu.pte.ttk.mii.fy0a83.mongothesis.Entities.Post;
import hu.pte.ttk.mii.fy0a83.mongothesis.Repositories.CustomPostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.TextCriteria;
import org.springframework.data.mongodb.core.query.TextQuery;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CustomPostRepositoryImpl implements CustomPostRepository {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public List<Post> upvotes() {

        Query query = new Query();

        query.with(new Sort(Sort.Direction.DESC, "social.upvotes")).limit(5);

        return mongoTemplate.find(query, Post.class);
    }

    @Override
    public List<Post> searchByKeyword(String keyword) {

        TextCriteria criteria = new TextCriteria();

        criteria.matchingAny(keyword);
        criteria.caseSensitive(false);
        criteria.diacriticSensitive(false);

        TextQuery query = new TextQuery(criteria);

        return mongoTemplate.find(query, Post.class);
    }
}
