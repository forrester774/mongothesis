package hu.pte.ttk.mii.fy0a83.mongothesis.ManagedBeans;


import hu.pte.ttk.mii.fy0a83.mongothesis.Entities.Post;
import hu.pte.ttk.mii.fy0a83.mongothesis.Repositories.CustomPostRepository;
import hu.pte.ttk.mii.fy0a83.mongothesis.Repositories.PostRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StopWatch;

import javax.annotation.PostConstruct;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;

@Named
@ViewScoped
public class HomeManagedBean {

    @Getter
    @Autowired
    private PostRepository postRepository;

    @Autowired
    private CustomPostRepository customPostRepository;

    @Setter
    @Getter
    private List<Post> postsList;

    @Getter
    private double loadPostsTime;

    @Getter
    private double findPostsByTopicsTime;

    @Getter
    private double mostLikedTime;

    @Getter
    private List<Post> postsByTopic;

    @Getter
    private List<Post> mostLiked;


    @PostConstruct
    public void init() {
        postsList = new ArrayList<>();
        loadPosts();
        postsByTopic = new ArrayList<>();
        mostLiked = new ArrayList<>();
    }

    private void loadPosts() {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        postsList = postRepository.findAllByOrderByCreatedDateDesc();
        stopWatch.stop();
        loadPostsTime = stopWatch.getTotalTimeSeconds();
    }

    public void homeNav() throws Exception{
        FacesContext.getCurrentInstance().getExternalContext().redirect("http://mongothesis-env.qkvfhgmmym.eu-west-1.elasticbeanstalk.com/index.xhtml");
    }

    public void findPostsByTopics(String topic) {
        try {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            postsByTopic = postRepository.findAllByTopicsContainsOrderByCreatedDate(topic);
            stopWatch.stop();
            findPostsByTopicsTime = stopWatch.getTotalTimeSeconds();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void mostLiked() {
        try {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            mostLiked = customPostRepository.upvotes();
            stopWatch.stop();
            mostLikedTime = stopWatch.getTotalTimeSeconds();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
